from setuptools import setup, find_packages

setup(
    name="wzlobbyseeker",
    version="1.1",
    packages=["wzlobbyseeker"],
    entry_points = {
        'console_scripts': [
            'wzlobbyseeker = wzlobbyseeker.main:main',                  
        ],              
    },
    install_requires=['beautifultable'],
)