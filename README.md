# warzone2100-lobbyseeker

A small program to get a list of Warzone 2100 servers via the lobby TCP protocol.

## Install

```sh
git clone <this repo> wzlobbyseeker
cd wzlobbyseeker
pip3 install .
```

## Example

The output of `wzlobbyseeker` in terminal.

```
╔════╤═══════════════════╤═══════════════╤════════════╤════════════╤═══════════╗
║    │       Name        │      Map      │    Host    │  Players   │ Modifiers ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 1  │      Welcome      │ DA-julia-4-v1 │ Autohoster │ 0 / 4(14)  │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 2  │      Welcome      │ DA-julia-40oi │ Autohoster │ 0 / 6(14)  │           ║
║    │                   │      l6p      │            │            │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 3  │      Welcome      │  NTWGlory-v2  │ Autohoster │ 0 / 8(14)  │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 4  │      Welcome      │    Vision     │ Autohoster │ 0 / 2(14)  │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 5  │   Wave defence    │ Stone-Jungle- │ Autohoster │ 1 / 10(14) │           ║
║    │                   │    v3w-T1     │            │            │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 6  │      Welcome      │ Tiny_VautEdit │ Autohoster │ 0 / 2(14)  │           ║
║    │                   │      ion      │            │            │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 7  │      Welcome      │  DA-Julia1v1  │ Autohoster │ 0 / 2(14)  │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 8  │ bhest BRazzerzs t │ NTW_4x4_Full- │ THEupreme  │ 7 / 8(14)  │           ║
║    │       hop 1       │      T1       │            │            │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 9  │    Just a game    │  NTW-Full2v2  │ BeginMadne │ 1 / 4(14)  │           ║
║    │                   │               │     ss     │            │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 10 │  bots_ 3v3_hard   │ R_LordsofWar  │    LX2     │ 6 / 6(14)  │           ║
╟────┼───────────────────┼───────────────┼────────────┼────────────┼───────────╢
║ 11 │ private game lol  │    Sk-Rush    │ Komanduius │ 1 / 4(14)  │           ║
║    │                   │               │   hchii    │            │           ║
╚════╧═══════════════════╧═══════════════╧════════════╧════════════╧═══════════╝
Welcome! The latest version of Warzone 2100 is 4.3.5
Download @ https://wz2100.net
Last hosted game was  0 hours,  0 mins, and  0 secs ago.
```
