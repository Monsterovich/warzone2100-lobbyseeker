import socket
from beautifultable import BeautifulTable,Style

HOST = "lobby.wz2100.net"
PORT = 9990

# Protocol constants
STRING_SIZE = 64
EXTRA_STRING_SIZE = 157
MAP_STRING_SIZE = 40
HOSTNAME_STRING_SIZE = 40
MODLIST_STRING_SIZE = 255


def main():
    # Init socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    sock.settimeout(2.5)
    sock.connect((socket.gethostbyname(HOST), PORT))
    sock.send(bytearray(b'list\0'))

    # Helper function to recv n bytes or return None if EOF is hit
    def recvall(n):
        data = bytearray()
        while len(data) < n:
            packet = sock.recv(n - len(data))
            if not packet:
                return None
            data.extend(packet)
        return data

    # Shorthands
    def recvint32():
        return int.from_bytes(recvall(4), "big")

    def recvint16():
        return int.from_bytes(recvall(2), "big")

    def recvstr(size):
        return recvall(size).decode("latin-1").rstrip("\x00")

    def recvgame():
        go = {}
        go["gamestruct_version"] = recvint32()
        go["name"] = recvstr(STRING_SIZE)
        # SESSIONDESC start
        recvint32() # dwFlags
        recvint32() # dwSize
        go["host"] = recvstr(40)
        go["max_players"] = recvint32()
        go["current_players"] = recvint32()
        go["spectators"] = recvint32() # always returns 14, useless
        recvint32() # game type
        recvint32() # unused in protocol
        recvint32() # unused in protocol
        # SESSIONDESC end
        go["secondary_hosts"] = recvall(80)
        go["extra"] = recvstr(EXTRA_STRING_SIZE)
        go["port"] = recvint16()
        go["map"] = recvstr(MAP_STRING_SIZE)
        go["hostname"] = recvstr(HOSTNAME_STRING_SIZE)
        go["version"] = recvstr(STRING_SIZE)
        go["mods"] = recvall(MODLIST_STRING_SIZE)
        recvint32() # major game version
        recvint32() # minor game version
        go["private"] = recvint32()
        go["map_has_mods"] = recvint32()
        recvint32() # number of mods
        recvint32() # gameId
        recvint32() # limits bit mask, always returns zero, doesn't work
        # kinda unused but not really
        go["platform"] = recvint32()  # platform (windows, mac, linux)
        recvint32() # constant value: NETCODE_VERSION_MAJOR << 16 | NETCODE_VERSION_MINOR
        return go

    # Retrieve number of games (11 games max before motd)
    num_games = recvint32()

    table = BeautifulTable()
    table.set_style(Style.STYLE_GRID)
    table.columns.header = ["Name", 
                            "Map", 
                            "Host", 
                            "Players", 
                            "Modifiers"]

    def addline(go):
        # "{}:{}".format(go["host"], go["port"])
        modifiers = ""
        if go["private"]:
            modifiers += "🔒"
        if go["map_has_mods"]:
            modifiers += "🔴"

        # if go["platform"] == 0x77696e:
        #     print("Windows")
        # elif go["platform"] == 0x6d6163:
        #     print("Mac")
        # else:
        #     print("Linux or else")

        table.rows.append([go["name"], 
                           go["map"], 
                           go["hostname"],
                           "{} / {}".format(go["current_players"],
                                                go["max_players"]),
                           modifiers])
        header.append((str(num + 1)))

    header = []

    # Retrieve game list (we are not going to print it because we'll get the full list after motd)
    for num in range(num_games):
        recvgame()

    recvint32() # wtf is this? it doesn't look like motd length

    # Retrieve message of the day
    message_length = recvint32()
    motd = recvstr(message_length)

    recvint32() # wtf is this too?

    #  Receive all games after motd
    num_games = recvint32()

    for num in range(num_games):
        go = recvgame()
        addline(go)

    if num_games == 0:
        print("No games found.")
    else:
        table.rows.header = header
        print(table)

    print(motd)

    sock.close()


if __name__ == "__main__":
    main()
